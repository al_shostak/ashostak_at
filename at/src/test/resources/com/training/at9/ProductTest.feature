@great
Feature: Search product

  Scenario:Running a Full Text Quick Search
    Given I opened www.ebay.com
    When I search the product "cosrx"
    And I add product to the cart
    Then "cosrx" appears in cart
