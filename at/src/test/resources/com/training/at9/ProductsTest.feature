@great
Feature: Search products in www.ebay.com

  Scenario Outline:Running a Full Text Quick Search
    Given I opened www.ebay.com
    When I search the product "<request>"
    Then the term query "<request>" should be the first in the Search Result grid

    Examples:
      | request     |
      | marc jacobs |
      | marvel      |
      | nike        |


