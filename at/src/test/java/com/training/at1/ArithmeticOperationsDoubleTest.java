package com.training.at1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class ArithmeticOperationsDoubleTest extends ConfigurationTest {
    private double a;
    private double b;

    @Factory(dataProvider = "dataCreator")
    public ArithmeticOperationsDoubleTest(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{-1.0, 12.7}, {45.0, 9.3}, {2.0, 3.7}};
    }

    @Test(priority = 0)
    public void testSum() {
        Double expected = a + b;
        Double actual = calculator.sum(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in sum(): " + a + "+" + b +
                " should be " + (a + b) + "]");
    }

    @Test(priority = 1)
    public void testSub() {
        Double expected = a - b;
        Double actual = calculator.sub(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in sub(): " + a + "-" + b +
                " should be " + (a - b) + "]");
    }

    @Test(priority = 2)
    public void testMult() {
        Double expected = a * b;
        Double actual = calculator.mult(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in mult(): " + a + "*" + b +
                " should be " + (a * b) + "]");
    }

    @Test(priority = 3)
    public void testDiv() {
        Double expected = a / b;
        Double actual = calculator.div(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in div()]");
    }

    @Test(priority = 4)
    public void testPow() {
        Double expected = Math.pow(a, b);
        Double actual = calculator.pow(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in pow(): " + a +
                " to the power of " + b + " should be " + (Math.pow(a, b)) + "]");
    }

    @Test(priority = 5)
    public void testSqrt() {
        Double expected = Math.sqrt(a);
        Double actual = calculator.sqrt(a);
        Assert.assertEquals(actual, expected, "[ERROR in sqrt()]");
    }
}
