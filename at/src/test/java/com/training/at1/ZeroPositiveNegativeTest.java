package com.training.at1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class ZeroPositiveNegativeTest extends ConfigurationTest {
    private long a;

    @Factory(dataProvider = "dataCreator")
    public ZeroPositiveNegativeTest(long a) {
        this.a = a;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{0}};
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testIsNegative() {
        Assert.assertFalse(calculator.isNegative(a), "[ERROR in isNegative(): " + a
                + " is neutral!!!]");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testIsPositive() {
        Assert.assertFalse(calculator.isPositive(a), "[ERROR in isPositive(): " + a
                + " is neutral!!!]");
    }
}
