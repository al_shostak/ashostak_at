package com.training.at1;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class ExpectedExceptionsLongTest extends ConfigurationTest {
    private long a;
    private long b;

    @Factory(dataProvider = "dataCreator")
    public ExpectedExceptionsLongTest(long a, long b) {
        this.a = a;
        this.b = b;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{1, 0}};
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void testDivideByZero() {
        calculator.div(a, b);
    }
}
