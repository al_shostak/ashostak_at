package com.training.at1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class ArithmeticOperationsLongTest extends ConfigurationTest {
    private long a;
    private long b;

    @Factory(dataProvider = "dataCreator")
    public ArithmeticOperationsLongTest(long a, long b) {
        this.a = a;
        this.b = b;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{0, 2}, {45, -4}};
    }

    @Test
    public void testSum() {
        Long expected = a + b;
        Long actual = calculator.sum(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in sum(): " + a + "+" + b +
                " should be " + (a + b) + "]");
    }

    @Test
    public void testSub() {
        Long expected = a - b;
        Long actual = calculator.sub(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in sub(): " + a + "-" + b +
                " should be " + (a - b) + "]");
    }

    @Test
    public void testMult() {
        Long expected = a * b;
        Long actual = calculator.mult(a, b);
        Assert.assertEquals(actual, expected, "[ERROR in mult(): " + a + "*" + b +
                " should be " + (a * b) + "]");
    }

    @Test
    public void testDiv() {
        Long actual = calculator.div(a, b);
        Long expected = a / b;
        Assert.assertEquals(actual, expected, "[ERROR in div(): " + a + "/" + b +
                " should be " + (a / b) + "]");
    }
}
