package com.training.at1;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class ExpectedExceptionsDoubleTest extends ConfigurationTest {
    private double a;
    private double b;

    @Factory(dataProvider = "dataCreator")
    public ExpectedExceptionsDoubleTest(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{-12.0, 0.0}};
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void testDivideByZero() {
        calculator.div(a, b);
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void testSqrtNegative() {
        calculator.sqrt(a);
    }
}

