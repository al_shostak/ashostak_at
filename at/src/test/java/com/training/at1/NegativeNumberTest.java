package com.training.at1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class NegativeNumberTest extends ConfigurationTest {
    private long a;

    @Factory(dataProvider = "dataCreator")
    public NegativeNumberTest(long a) {
        this.a = a;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{-12}};
    }

    @Test
    public void testIsNegative() {
        Assert.assertTrue(calculator.isNegative(a), "[ERROR in isNegative(): " + a
                + " is a positive number!!!]");
    }

    @Test
    public void testIsPositive() {
        Assert.assertFalse(calculator.isPositive(a), "[ERROR in isPositive(): " + a
                + " is a negative number!!!]");
    }
}
