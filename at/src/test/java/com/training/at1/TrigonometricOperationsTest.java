package com.training.at1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class TrigonometricOperationsTest extends ConfigurationTest {
    private double a;

    @Factory(dataProvider = "dataCreator")
    public TrigonometricOperationsTest(double a) {
        this.a = a;
    }

    @DataProvider
    public static Object[][] dataCreator() {
        return new Object[][]{{0.0}, {30.0}, {45.0}, {60.0}, {90.0}};
    }

    @Test
    public void testCos() {
        Double expected = Math.cos(a);
        Double actual = calculator.cos(a);
        Assert.assertEquals(actual, expected, "[ERROR in cos()! cos( " + a + " ) should be " +
                Math.cos(a) + " ]");
    }

    @Test
    public void testSin() {
        Double expected = Math.sin(a);
        Double actual = calculator.sin(a);
        Assert.assertEquals(actual, expected, "[ERROR in sin()! sin( " + a + " ) should be " +
                Math.sin(a) + " ]");
    }

    @Test
    public void testTg() {
        Double expected = Math.tan(a);
        Double actual = calculator.tg(a);
        Assert.assertEquals(actual, expected, "[ERROR in tg()! tan( " + a + " ) should be " +
                Math.tan(a) + " ]");
    }

    @Test
    public void testCtg() {
        Double expected = Math.atan(a);
        Double actual = calculator.ctg(a);
        Assert.assertEquals(actual, expected, "[ERROR in ctg()! ctg( " + a + " ) should be " +
                Math.atan(a) + " ]");
    }
}
