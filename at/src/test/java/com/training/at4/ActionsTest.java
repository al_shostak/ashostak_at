package com.training.at4;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ActionsTest extends ConfigurationTest {

    @Test(priority = 0)
    public void testDragAndDrop() {
        DroppablePage droppablePage = navigationMenu.openDroppablePage();
        String expected = "Dropped!";
        Assert.assertEquals(droppablePage.dragAndDrop(), expected);
    }

    @Test(priority = 1)
    public void testCheckBoxNY() {
        CheckBoxRadioPage checkBoxRadioPage = navigationMenu.openCheckBoxPage();
        String expected = "checked";
        Assert.assertTrue(checkBoxRadioPage.CheckNY().contains(expected));
    }

    @Test(priority = 2)
    public void testCheckBoxTwoStar() {
        CheckBoxRadioPage checkBoxRadioPage = navigationMenu.openCheckBoxPage();
        String expected = "checked";
        Assert.assertTrue(checkBoxRadioPage.CheckTwoStar().contains(expected));
    }

    @Test(priority = 3)
    public void testSelectSpeed() {
        SelectMenuPage selectMenuPage = navigationMenu.openSelectMenuPage();
        String expected = "Fast";
        Assert.assertEquals(selectMenuPage.selectSpeed(), expected);
    }

    @Test(priority = 4)
    public void testSelectNumber() {
        SelectMenuPage selectMenuPage = navigationMenu.openSelectMenuPage();
        String expected = "3";
        Assert.assertEquals(selectMenuPage.selectNumber(), expected);
    }

    @Test(priority = 5)
    public void testToolTip() {
        TooltipPage tooltipPage = navigationMenu.openTooltipPage();
        String expected = "That's what this widget is";
        Assert.assertEquals(tooltipPage.tooltipsText(), expected);
    }

    @Test(priority = 6)
    public void testQuickSearch() {
        TabsPage tabsPage = navigationMenu.openTabsPage();
        String expected = "Proin elit arcu";
        Assert.assertTrue(tabsPage.clickTab().contains(expected));
    }
}
