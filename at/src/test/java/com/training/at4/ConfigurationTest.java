package com.training.at4;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "http://jqueryui.com";
    protected NavigationMenu navigationMenu;

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
            navigationMenu = new NavigationMenu(driver);
        }
    }

    @BeforeMethod
    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    @AfterClass
    public void reset() {
        driver.quit();
        driver = null;
        navigationMenu = null;
    }
}
