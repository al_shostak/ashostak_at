package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TabsPage extends BasePage {
    private static final By I_FRAME = By.className("demo-frame");
    private static final By TAB = By.linkText("Nunc tincidunt");
    private static final By TEXT = By.xpath("//div[@id='tabs-1']/p");

    public TabsPage(WebDriver driver) {
        super(driver);
    }

    public String clickTab(){
        driver.switchTo().frame(driver.findElement(I_FRAME));
        driver.findElement(TAB).click();
        return driver.findElement(TEXT).getText();
    }
}
