package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckBoxRadioPage extends BasePage {
    private static final By I_FRAME = By.className("demo-frame");
    private static final By RADIO_NY = By.xpath("//label[@for='radio-1']");
    private static final By RADIO_TWO_STAR = By.xpath("//label[@for='checkbox-1']");
    private static final String CLASS = "class";

    public CheckBoxRadioPage(WebDriver driver) {
        super(driver);
    }

    public String CheckNY() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        driver.findElement(RADIO_NY).click();
        return driver.findElement(RADIO_NY).getAttribute(CLASS);
    }

    public String CheckTwoStar() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        driver.findElement(RADIO_TWO_STAR).click();
        return driver.findElement(RADIO_TWO_STAR).getAttribute(CLASS);
    }
}
