package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends BasePage {
    private static final By DROPPABLE = By.xpath("//div[@id='droppable']");
    private static final By DRAGGABLE = By.xpath("//div[@id='draggable']");
    private static final By I_FRAME = By.className("demo-frame");

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public String dragAndDrop() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        Actions action = new Actions(driver);
        action.dragAndDrop(driver.findElement(DRAGGABLE), driver.findElement(DROPPABLE)).
                build().
                perform();
        return driver.findElement(DROPPABLE).getText();
    }
}

