package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends BasePage {
    private static final By I_FRAME = By.className("demo-frame");
    private static final By TOOL_TIP = By.xpath("//a[@href='#']");
    private static final String TITLE = "title";

    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    public String tooltipsText() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(TOOL_TIP)).perform();
        driver.findElement(TOOL_TIP).click();
        return driver.findElement(TOOL_TIP).getAttribute(TITLE);
    }
}
