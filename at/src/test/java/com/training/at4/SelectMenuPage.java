package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SelectMenuPage extends BasePage {
    private static final By I_FRAME = By.className("demo-frame");
    private static final By SECTION_SPEED = By.id("speed-button");
    private static final By FAST_VALUE = By.id("ui-id-4");
    private static final By SECTION_NUMBER = By.id("number-button");
    private static final By THREE_VALUE = By.id("ui-id-3");
    private static final By FAST = By.xpath("//span[@id='speed-button']/span[2]");
    private static final By NUMBER = By.xpath("//span[@id='number-button']/span[2]");

    public SelectMenuPage(WebDriver driver) {
        super(driver);
    }

    public String selectSpeed() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        driver.findElement(SECTION_SPEED).click();
        driver.findElement(FAST_VALUE).click();
        return driver.findElement(FAST).getText();
    }

    public String selectNumber() {
        driver.switchTo().frame(driver.findElement(I_FRAME));
        driver.findElement(SECTION_NUMBER).click();
        driver.findElement(THREE_VALUE).click();
        return driver.findElement(NUMBER).getText();
    }
}
