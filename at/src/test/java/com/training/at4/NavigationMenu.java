package com.training.at4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavigationMenu extends BasePage {
    private static final By DROPPABLE = By.linkText("Droppable");
    private static final By CHECKBOXRADIO = By.linkText("Checkboxradio");
    private static final By SELECTMENU = By.linkText("Selectmenu");
    private static final By TOOLTIP = By.linkText("Tooltip");
    private static final By TABS = By.linkText("Tabs");

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    public DroppablePage openDroppablePage() {
        driver.findElement(DROPPABLE).click();
        return new DroppablePage(driver);
    }

    public CheckBoxRadioPage openCheckBoxPage() {
        driver.findElement(CHECKBOXRADIO).click();
        return new CheckBoxRadioPage(driver);
    }

    public SelectMenuPage openSelectMenuPage() {
        driver.findElement(SELECTMENU).click();
        return new SelectMenuPage(driver);
    }

    public TooltipPage openTooltipPage() {
        driver.findElement(TOOLTIP).click();
        return new TooltipPage(driver);
    }

    public TabsPage openTabsPage() {
        driver.findElement(TABS).click();
        return new TabsPage(driver);
    }
}
