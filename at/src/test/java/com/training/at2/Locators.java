package com.training.at2;

import org.openqa.selenium.By;

public class Locators {
    protected static final By LOGIN_BUTTON = By.xpath("//a[contains(@class,'button desk')]");
    protected static final By LOGIN_BOX = By.id("passp-field-login");
    protected static final By SUBMIT_BUTTON = By.xpath("//button[contains(@type,'submit')]");
    protected static final By PASSWORD = By.id("passp-field-passwd");
    protected static final By WRITE_EMAIL = By.cssSelector("a[class^=\"mail-ComposeButton\"]");
    protected static final By WRITE_TO = By.name("to");
    protected static final By THEME_EMAIL = By.cssSelector("input[class^=\"mail-Compose-Field\"]");
    protected static final By TEXTBOX = By.cssSelector("textarea[dir]");
    protected static final By DRAFT = By.xpath("//span[text()='Черновики']");
    protected static final By SAVE_AND_GO = By.xpath("//span[text()='Сохранить и перейти']");
    protected static final By VERIFY_ADDRESS = By.cssSelector("span[class^=\"mail-MessageSnippet-FromText\"]");
    protected static final By SEND_BUTTON = By.xpath("//span[text()='Отправить']");
    protected static final By SENT_EMAILS = By.xpath("//span[text()='Отправленные']");
    protected static final By RECIPIENT = By.cssSelector("#recipient-1");
    protected static final By SIGN_OUT = By.linkText("Выйти из сервисов Яндекса");
    protected static final By MY_EMAIL = By.cssSelector("span[title*='Hi']");
    protected static final By MY_THEME_EMAIL = By.cssSelector("input[value*='Hi']");
    protected static final By IMG_EMAIL_IS_SENT = By.xpath("//div[text()='Письмо отправлено.']");

}

