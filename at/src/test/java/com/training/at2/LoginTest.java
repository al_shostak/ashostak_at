package com.training.at2;

import org.testng.Assert;
import org.testng.annotations.Test;
import static com.training.at2.Locators.LOGIN_BUTTON;

public class LoginTest extends ConfigurationTest {
    private static final String LOGIN = "at.training2019";
    private static final String PASSWORD = "at.training";

    @Test
    public void testLogIn(){
        driver.findElement(LOGIN_BUTTON).click();
        driver.findElement(Locators.LOGIN_BOX).clear();
        driver.findElement(Locators.LOGIN_BOX).sendKeys(LOGIN);
        driver.findElement(Locators.SUBMIT_BUTTON).click();
        driver.findElement(Locators.PASSWORD).clear();
        driver.findElement(Locators.PASSWORD).sendKeys(PASSWORD);
        driver.findElement(Locators.SUBMIT_BUTTON).click();
        Assert.assertFalse(elementIsPresent(LOGIN_BUTTON),"Login is not successful.");
    }
}
