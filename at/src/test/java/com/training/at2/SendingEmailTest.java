package com.training.at2;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import static com.training.at2.Locators.*;

public class SendingEmailTest extends ConfigurationTest {
    private static final String LOGIN = "at.training2019";
    private static final String PASSWORD = "at.training";
    private static final String THEME = "Hi";
    private static final String TEXT = "Hello world";
    private static final String ADDRESS = "epam@yandex.by";
    private static final String EPAM = "epam";
    private static final String VALUE = "value";
    private static final String TITLE = "title";

    @Test
    public void testSendingEmail() {
        driver.findElement(Locators.LOGIN_BUTTON).click();
        driver.findElement(Locators.LOGIN_BOX).clear();
        driver.findElement(Locators.LOGIN_BOX).sendKeys(LOGIN);
        driver.findElement(Locators.SUBMIT_BUTTON).click();
        driver.findElement(Locators.PASSWORD).clear();
        driver.findElement(Locators.PASSWORD).sendKeys(PASSWORD);
        driver.findElement(Locators.SUBMIT_BUTTON).click();
        Assert.assertFalse(elementIsPresent(LOGIN_BUTTON),"Login is not successful.");
        driver.findElement(Locators.WRITE_EMAIL).click();
        //Create a unique theme for email.
        String theme = THEME + System.currentTimeMillis();
        driver.findElement(Locators.WRITE_TO).sendKeys(ADDRESS);
        driver.findElement(Locators.THEME_EMAIL).sendKeys(theme);
        driver.findElement(Locators.TEXTBOX).sendKeys(TEXT);
        //Go to the draft folder.
        driver.findElement(Locators.DRAFT).click();
        driver.findElement(Locators.SAVE_AND_GO).click();
        driver.navigate().refresh();
        Assert.assertTrue(isMailInTheFolder(MY_EMAIL, TITLE, theme),
                "Mail does not presents in ‘Drafts’ folder.");
        Assert.assertTrue(driver.findElement(VERIFY_ADDRESS).getText().contains(EPAM),
                "[Address is not the same!]");
        Assert.assertEquals(theme, getAttribute(MY_THEME_EMAIL, VALUE),
                "[Theme is not the same!]");
        //Send the email.
        driver.findElement(Locators.SEND_BUTTON).click();
        driver.findElement(IMG_EMAIL_IS_SENT);
        //Go to the folder sent emails.
        driver.findElement(Locators.SENT_EMAILS).click();
        Assert.assertTrue(isMailInTheFolder(MY_EMAIL, TITLE, theme),
                "The mail is not in ‘Sent’ folder");
        driver.findElement(Locators.DRAFT).click();
        driver.navigate().refresh();
        Assert.assertFalse(isMailInTheFolder(MY_EMAIL, TITLE, theme),"The mail is in the ‘Drafts’ folder.");
        //LOGOUT
        new WebDriverWait(driver, 20).
                until(ExpectedConditions.visibilityOfElementLocated(RECIPIENT));
        driver.findElement(RECIPIENT).click();
        driver.findElement(SIGN_OUT).click();
        Assert.assertTrue(elementIsPresent(LOGIN_BUTTON),"Log off is not successful.");
    }
}
