package com.training.at2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://yandex.by/";

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
        }
    }

    protected boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    protected boolean isMailInTheFolder(By locator, String attrName, String text) {
        boolean isPresent = false;
        List<WebElement> webElements = driver.findElements(locator);
        for (WebElement webElement : webElements) {
            if (webElement.getAttribute(attrName).equals(text)) {
                webElement.click();
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    protected String getAttribute(By locator, String name) {
        WebElement webElement = driver.findElement(locator);
        return webElement.getAttribute(name);
    }

    @AfterClass
    public void reset() {
        driver.quit();
        driver = null;
    }
}
