package com.training.at8;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class HttpClientTask {
    private static final String BASE_URL = "http://jsonplaceholder.typicode.com/albums/";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";
    private static final Integer ID = 5;
    private static final String TITLE = "album test";
    private static final String UPDATE_TITLE = "album test update";
    private HttpClient client = HttpClients.createDefault();

    @Test
    public void testVerifyStatusCode(){
        HttpGet request = new HttpGet(BASE_URL);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
    }

    @Test
    public void testVerifyHeader(){
        HttpGet request = new HttpGet(BASE_URL);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(response.containsHeader(CONTENT_TYPE));
        Header[] headers = response.getHeaders(CONTENT_TYPE);
        assertEquals(headers[0].getValue(), CONTENT_TYPE_VALUE);
    }

    @Test
    public void testNumberOfAlbums() {
        HttpGet request = new HttpGet(BASE_URL);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String json = null;
        try {
            json = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArray = new JSONArray(json);
        Assert.assertEquals(jsonArray.length(), 100);
    }

    @Test
    public void testVerifyContentBody(){
        HttpGet request = new HttpGet(BASE_URL + ID);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseHandler<String> handler = new BasicResponseHandler();
        String body = null;
        try {
            body = handler.handleResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(body.contains("id"));
        Assert.assertTrue(body.contains("userId"));
        Assert.assertTrue(body.contains("title"));
    }

    @Test
    public void testCreateAlbum(){
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(BASE_URL);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userId", "1"));
        params.add(new BasicNameValuePair("title", TITLE));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String json = null;
        try {
            json = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject(json);
        assertThat(response.getStatusLine().getStatusCode(), equalTo(201));
        System.out.println("Id of new album is " + jsonObject.getInt("id"));
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateAlbum(){
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(BASE_URL + ID);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userId", "1"));
        params.add(new BasicNameValuePair("title", UPDATE_TITLE));
        try {
            httpPut.setEntity(new UrlEncodedFormEntity(params));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpPut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String json = null;
        try {
            json = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject(json);
        assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
        assertThat(jsonObject.getInt("id"),equalTo(ID));
        System.out.println("Id of updated album is " + jsonObject.getInt("id"));
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteAlbum() {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpDelete httpDelete = new HttpDelete(BASE_URL + ID);
        CloseableHttpResponse response = null;
        try {
            response = client.execute(httpDelete);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
