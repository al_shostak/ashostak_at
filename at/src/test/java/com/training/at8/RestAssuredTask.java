package com.training.at8;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.LinkedHashMap;
import java.util.Map;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RestAssuredTask {
    private static final String BASE_URL = "http://jsonplaceholder.typicode.com/albums/";
    private static final Integer ARRAY_SIZE = 100;
    private static final Integer USER_ID = 1;
    private static final Integer ID = 5;
    private static final String ALBUM_TEST = "album test";
    private static final String TITLE = "eaque aut omnis a";
    private static final String UPDATE_TITLE = "album test update";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";

    @Test
    public void testVerifyStatusCode() {
        given().log().all().when().get(BASE_URL)
                .then().assertThat().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testVerifyHeader() {
        given().when().get(BASE_URL)
                .then().log().headers().assertThat().statusCode(HttpStatus.SC_OK)
                .and().assertThat().contentType(ContentType.JSON)
                .and().header(CONTENT_TYPE, equalTo(CONTENT_TYPE_VALUE));
    }

    @Test
    public void testVerifyBody() {
        given().when().get(BASE_URL)
                .then().log().all().assertThat().body("size()", is(ARRAY_SIZE));
    }

    @Test
    public void testVerifyContentBody() {
        given().when().get(BASE_URL + ID)
                .then().assertThat().log().body().body("userId", equalTo(USER_ID),
                "id", equalTo(ID),
                "title", equalTo(TITLE));
    }

    @Test
    public void testCreateAlbum() {
        Map<String, Object> album = new LinkedHashMap<>();
        album.put("userId", USER_ID);
        album.put("title", ALBUM_TEST);
        Response response = given().accept(ContentType.JSON).contentType(ContentType.JSON).body(album)
                .when().post(BASE_URL)
                .then().log().body().assertThat().statusCode(HttpStatus.SC_CREATED)
                .contentType(ContentType.JSON).extract().response();
        Assert.assertFalse(response.jsonPath().getString("userId").isEmpty());
        Assert.assertEquals(ALBUM_TEST, response.jsonPath().getString("title"));
        System.out.println("Id of new album is  " + response.jsonPath().getString("id"));
    }

    @Test
    public void testUpdateAlbum() {
        Map<String, Object> album = new LinkedHashMap<>();
        album.put("userId", USER_ID);
        album.put("title", UPDATE_TITLE);
        given().accept(ContentType.JSON).contentType(ContentType.JSON).body(album)
                .when().put(BASE_URL + ID)
                .then().log().body().assertThat().statusCode(HttpStatus.SC_OK)
                .assertThat().body("id", equalTo(ID))
                .contentType(ContentType.JSON).extract().response();
    }

    @Test
    public void testDeleteAlbum() {
        given().when().delete(BASE_URL + ID)
                .then().log().ifStatusCodeIsEqualTo(HttpStatus.SC_OK);
    }
}
