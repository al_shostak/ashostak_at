package com.training.at8;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class RestTemplateTasк {
    private static final String BASE_URL = "http://jsonplaceholder.typicode.com/albums/";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";
    private static final Integer USER_ID = 1;
    private static final Integer ID = 5;
    private static final String TITLE = "album test";
    private static final String TITLE_1 = "eaque aut omnis a";
    private static final String UPDATE_TITLE = "album test update";

    @Test
    public void testVerifyStatusCode() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(BASE_URL, String.class);
        assertEquals(response.getStatusCode().value(), HttpStatus.SC_OK);
    }

    @Test
    public void testVerifyHeader() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = restTemplate.headForHeaders(BASE_URL);
        assertFalse(httpHeaders.isEmpty());
        assertTrue(httpHeaders.get(CONTENT_TYPE).contains(CONTENT_TYPE_VALUE));
    }

    @Test
    public void testNumberOfAlbums() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(BASE_URL, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        assertEquals(root.size(), 100);
    }

    @Test
    public void testVerifyContentBody() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(BASE_URL + ID, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        JsonNode id = root.path("id");
        Integer id_1 = id.intValue();
        assertEquals(id_1, ID);

        JsonNode userId = root.path("userId");
        Integer userId_1 = userId.intValue();
        assertEquals(userId_1, USER_ID);

        JsonNode title = root.path("title");
        String title_1 = title.textValue();
        assertEquals(title_1, TITLE_1);
    }

    @Test
    public void testCreateAlbum() {
        RestTemplate restTemplate = new RestTemplate();
        JSONObject request = new JSONObject();
        request.put("userId", USER_ID);
        request.put("title", TITLE);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

        ResponseEntity<String> responseEntity = restTemplate
                .exchange(BASE_URL, HttpMethod.POST, entity, String.class);
        int statusCode = responseEntity.getStatusCode().value();
        assertEquals(statusCode, HttpStatus.SC_CREATED);
        assertTrue(responseEntity.hasBody());
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(responseEntity.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println("Id of new album is - " + root.path("id"));
    }

    @Test
    public void testUpdateAlbum() {
        RestTemplate restTemplate = new RestTemplate();
        JSONObject request = new JSONObject();
        request.put("userId", USER_ID);
        request.put("title", UPDATE_TITLE);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(BASE_URL + ID, HttpMethod.PUT, entity, String.class);
        assertEquals(responseEntity.getStatusCode().value(), HttpStatus.SC_OK);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(responseEntity.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        JsonNode id = root.path("id");
        Integer id_1 = id.intValue();
        assertEquals(id_1, ID);
        System.out.println("Id in the body = " + id);
    }

    @Test
    public void testDeleteAlbum() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(BASE_URL + ID);
        ResponseEntity<String> response = restTemplate.getForEntity(BASE_URL + ID, String.class);
        assertEquals(response.getStatusCode().value(), HttpStatus.SC_OK);
    }
}
