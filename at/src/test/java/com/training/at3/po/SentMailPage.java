package com.training.at3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentMailPage extends Page {
    protected static final By MY_EMAIL = By.cssSelector("span[title*='Hi']");

    public SentMailPage(WebDriver driver) {
        super(driver);
    }
}
