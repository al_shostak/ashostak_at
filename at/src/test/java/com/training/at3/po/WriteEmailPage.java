package com.training.at3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteEmailPage extends Page {
    private static final By WRITE_TO = By.name("to");
    private static final By THEME_EMAIL = By.cssSelector("input[class^=\"mail-Compose-Field\"]");
    private static final By TEXTBOX = By.cssSelector("textarea[dir]");
    private static final By SEND_BUTTON = By.xpath("//span[text()='Отправить']");
    private static final By SAVE_AND_GO = By.xpath("//span[text()='Сохранить и перейти']");
    private static final By DRAFT_BUTTON = By.xpath("//span[text()='Черновики']");
    private static final By IMG_EMAIL_IS_SENT = By.xpath("//div[text()='Письмо отправлено.']");

    public WriteEmailPage(WebDriver driver) {
        super(driver);
    }

    public WriteEmailPage enterTo(String address) {
        driver.findElement(WRITE_TO).sendKeys(address);
        return new WriteEmailPage(driver);
    }

    public String enterTheme(String theme) {
        String newTheme = theme + System.currentTimeMillis();
        driver.findElement(THEME_EMAIL).sendKeys(newTheme);
        return newTheme;
    }

    public WriteEmailPage enterText(String text) {
        driver.findElement(TEXTBOX).sendKeys(text);
        return new WriteEmailPage(driver);
    }

    public DraftMailPage saveEmailToDraft() {
        driver.findElement(DRAFT_BUTTON).click();
        driver.findElement(SAVE_AND_GO).click();
        driver.navigate().refresh();
        return new DraftMailPage(driver);
    }

    public NavigationMenuPage submitSendButton() {
        driver.findElement(SEND_BUTTON).click();
        driver.findElement(IMG_EMAIL_IS_SENT);
        return new NavigationMenuPage(driver);
    }
}

