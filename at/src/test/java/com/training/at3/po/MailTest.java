package com.training.at3.po;

import org.testng.Assert;
import org.testng.annotations.*;

public class MailTest extends ConfigurationTest {
    private static final String EMAIL = "at.training2019";
    private static final String PASSWORD = "at.training";
    private static final String ADDRESSTO = "minsk@yandex.by";
    private static final String PARTOFADDRESS = "minsk";
    private static final String THEME = "Hi";
    private static final String TEXT = "Hello everybody!";
    private static final String VALUE = "value";
    private static final String TITLE = "title";
    private LogInPage logInPage;
    private NavigationMenuPage navigationMenuPage;
    private WriteEmailPage writeEmailPage;

    @BeforeClass
    public void init(){
        logInPage = new LogInPage(driver);
        navigationMenuPage = new NavigationMenuPage(driver);
        writeEmailPage = new WriteEmailPage(driver);
    }

    @Test(priority = 0)
    public void testLogIn() {
        logInPage.submitLogInButton().
                enterEmail(EMAIL).
                enterPassword(PASSWORD);
        Assert.assertFalse(elementIsPresent(LogInPage.LOGIN_BUTTON),"Login is not successful.");
    }

    @Test(priority = 1)
    public void testCreateAndSaveMail() {
                navigationMenuPage.submitWriteEmailButton().
                enterTo(ADDRESSTO);
        String theme = writeEmailPage.enterTheme(THEME);
        writeEmailPage.enterText(TEXT).
                saveEmailToDraft();
        Assert.assertTrue(isMailInTheFolder(DraftMailPage.MY_EMAIL, TITLE, theme),
                "Mail does not present in ‘Drafts’ folder!");
        Assert.assertTrue(driver.findElement(DraftMailPage.VERIFY_ADDRESS).getText().contains(PARTOFADDRESS),
                "[Address is not the same!]");
        Assert.assertEquals(theme, getAttribute(DraftMailPage.MY_THEME_EMAIL, VALUE),
                "[Theme is not the same!]");
    }

    @Test(priority = 2)
    public void sendMail() {
        String savedMailTheme = getAttribute(DraftMailPage.MY_THEME_EMAIL,VALUE);
        writeEmailPage.submitSendButton();
        navigationMenuPage.submitSentButton();
        Assert.assertTrue(isMailInTheFolder(SentMailPage.MY_EMAIL, TITLE, savedMailTheme),
                "Mail does not present in ‘Sent’ folder!");
        navigationMenuPage.submitDraftButton();
        driver.navigate().refresh();
        Assert.assertFalse(isMailInTheFolder(DraftMailPage.MY_EMAIL, TITLE, savedMailTheme),
                "Mail presents in ‘Draft’ folder!");
    }

    @Test(priority = 3)
    public void logOut() {
        navigationMenuPage.logOut();
        Assert.assertTrue(elementIsPresent(LogInPage.LOGIN_BUTTON),"Logout is not successful.");
    }

    @AfterClass
    public void clearDown(){
        logInPage = null;
        navigationMenuPage = null;
        writeEmailPage = null;
    }
}
