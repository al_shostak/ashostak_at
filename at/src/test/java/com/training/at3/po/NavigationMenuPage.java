package com.training.at3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NavigationMenuPage extends Page {
    private final static By WRITE_EMAIL_BUTTON = By.cssSelector("a[class^=\"mail-ComposeButton\"]");
    private final static By DRAFT_BUTTON = By.xpath("//span[text()='Черновики']");
    private final static By SENT_EMAILS = By.xpath("//span[text()='Отправленные']");
    private final static By RECIPIENT = By.cssSelector("#recipient-1");
    private final static By SIGN_OUT = By.linkText("Выйти из сервисов Яндекса");

    public NavigationMenuPage(WebDriver driver) {
        super(driver);
    }

    public WriteEmailPage submitWriteEmailButton() {
        driver.findElement(WRITE_EMAIL_BUTTON).click();
        return new WriteEmailPage(driver);
    }

    public DraftMailPage submitDraftButton() {
        driver.findElement(DRAFT_BUTTON).click();
        return new DraftMailPage(driver);
    }

    public SentMailPage submitSentButton() {
        driver.findElement(SENT_EMAILS).click();
        driver.navigate().refresh();
        return new SentMailPage(driver);
    }

    public void logOut() {
        new WebDriverWait(driver, 20).
                until(ExpectedConditions.visibilityOfElementLocated(RECIPIENT));
        driver.findElement(RECIPIENT).click();
        driver.findElement(SIGN_OUT).click();
    }
}
