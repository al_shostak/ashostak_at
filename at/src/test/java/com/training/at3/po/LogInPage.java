package com.training.at3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogInPage extends Page {
    protected static final By LOGIN_BUTTON = By.xpath("//a[contains(@class,'button desk')]");
    private static final By ENTER_EMAIL = By.id("passp-field-login");
    private static final By SUBMIT_BUTTON = By.xpath("//button[contains(@type,'submit')]");
    private static final By PASSWORD = By.id("passp-field-passwd");

    public LogInPage(WebDriver driver) {
        super(driver);
    }

    public LogInPage submitLogInButton() {
        driver.findElement(LOGIN_BUTTON).click();
        return new LogInPage(driver);
    }

    public LogInPage enterEmail(String email) {
        driver.findElement(ENTER_EMAIL).clear();
        driver.findElement(ENTER_EMAIL).sendKeys(email);
        driver.findElement(SUBMIT_BUTTON).click();
        return new LogInPage(driver);
    }

    public NavigationMenuPage enterPassword(String myPassword) {
        driver.findElement(PASSWORD).clear();
        driver.findElement(PASSWORD).sendKeys(myPassword);
        driver.findElement(SUBMIT_BUTTON).click();
        return new NavigationMenuPage(driver);
    }
}
