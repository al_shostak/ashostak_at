package com.training.at3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftMailPage extends Page {
    protected static final By MY_EMAIL = By.cssSelector("span[title*='Hi']");
    protected static final By VERIFY_ADDRESS = By.cssSelector("span[class^=\"mail-MessageSnippet-FromText\"]");
    protected static final By MY_THEME_EMAIL = By.cssSelector("input[value*='Hi']");

    public DraftMailPage(WebDriver driver) {
        super(driver);
    }
}