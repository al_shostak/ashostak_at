package com.training.at3.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NavigationMenuPage extends Page {
    @FindBy(linkText = "Написать")
    protected static WebElement writeEmailButton;

    @FindBy(xpath = "//span[text()='Черновики']")
    private WebElement draftButton;

    @FindBy(xpath = "//span[text()='Отправленные']")
    private WebElement sentEmails;

    @FindBy(css = "#recipient-1")
    private WebElement recipient;

    @FindBy(linkText = "Выйти из сервисов Яндекса")
    private WebElement signOut;

    private static final String RECIPIENT = "#recipient-1";

    public NavigationMenuPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WriteEmailPage submitWriteEmailButton() {
        writeEmailButton.click();
        return new WriteEmailPage(driver);
    }

    public DraftMailPage submitDraftButton() {
        draftButton.click();
        return new DraftMailPage(driver);
    }

    public SentMailPage submitSentButton() {
        sentEmails.click();
        driver.navigate().refresh();
        return new SentMailPage(driver);
    }

    public void logOut(){
        new WebDriverWait(driver, 20).
                until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(RECIPIENT)));
        recipient.click();
        signOut.click();
    }
}

