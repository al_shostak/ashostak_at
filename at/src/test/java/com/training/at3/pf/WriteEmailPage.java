package com.training.at3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WriteEmailPage extends Page {
    @FindBy(name = "to")
    private WebElement writeTo;

    @FindBy(css = "input[class^=\"mail-Compose-Field\"]")
    private WebElement themeEmail;

    @FindBy(css = "textarea[dir]")
    private WebElement textbox;

    @FindBy(xpath = "//span[text()='Отправить']")
    private WebElement sendButton;

    @FindBy(xpath = "//span[text()='Сохранить и перейти']")
    private WebElement saveAndGo;

    @FindBy(xpath = "//span[text()='Черновики']")
    private WebElement draftButton;

    @FindBy(xpath = "//div[text()='Письмо отправлено.']")
    private WebElement imgEmailIsSent;

    public WriteEmailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WriteEmailPage enterTo(String address) {
        writeTo.sendKeys(address);
        return new WriteEmailPage(driver);
    }

    public String enterTheme(String theme) {
        String newTheme = theme + System.currentTimeMillis();
        themeEmail.sendKeys(newTheme);
        return newTheme;
    }

     public WriteEmailPage enterText(String text) {
         textbox.sendKeys(text);
         return new WriteEmailPage(driver);
     }

    public DraftMailPage saveEmailToDraft() {
        draftButton.click();
        saveAndGo.click();
        driver.navigate().refresh();
        return new DraftMailPage(driver);
    }

    public NavigationMenuPage submitSendButton() {
        sendButton.click();
        imgEmailIsSent.isEnabled();
        return new NavigationMenuPage(driver);
    }
}