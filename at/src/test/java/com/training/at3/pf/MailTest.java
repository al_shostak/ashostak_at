package com.training.at3.pf;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MailTest extends ConfigurationTest {
    private static final String EMAIL = "at.training2019";
    private static final String PASSWORD = "at.training";
    private static final String ADDRESSTO = "epam@yandex.by";
    private static final String PARTOFADDRESS = "epam";
    private static final String THEME = "Hi";
    private static final String TEXT = "Hello everybody!";
    private static final String VALUE = "value";
    private static final String TITLE = "title";
    private static final String PATH = "span[title*='Hi']";
    private LogInPage logInPage;
    private NavigationMenuPage navigationMenuPage;
    private WriteEmailPage writeEmailPage;

    @BeforeClass
    public void init() {
        logInPage = new LogInPage(driver);
        navigationMenuPage = new NavigationMenuPage(driver);
        writeEmailPage = new WriteEmailPage(driver);
    }

    @Test(priority = 0)
    public void testLogIn() {
        logInPage.submitLogInButton().
                enterEmail(EMAIL).
                enterPassword(PASSWORD);
        Assert.assertTrue(NavigationMenuPage.writeEmailButton.isDisplayed(), "Login is not successful.");
    }

    @Test(priority = 1)
    public void testCreateAndSaveMail() {
        navigationMenuPage.submitWriteEmailButton().
                enterTo(ADDRESSTO);
        String theme = writeEmailPage.enterTheme(THEME);
        writeEmailPage.enterText(TEXT).
                saveEmailToDraft();
        Assert.assertTrue(isMailInTheFolder(PATH, TITLE, theme),
                "Mail does not present in ‘Drafts’ folder!");
        Assert.assertTrue(DraftMailPage.verifyAddress.getText().contains(PARTOFADDRESS),
                "[Address is not the same!]");
        Assert.assertEquals(theme, DraftMailPage.myThemeEmail.getAttribute(VALUE),
                "[Theme is not the same!]");
    }

    @Test(priority = 2)
    public void testSendMail() {
        String savedMailTheme = DraftMailPage.myThemeEmail.getAttribute(VALUE);
        writeEmailPage.submitSendButton();
        navigationMenuPage.submitSentButton();
        Assert.assertTrue(isMailInTheFolder(PATH, TITLE, savedMailTheme),
                "Mail does not present in ‘Sent’ folder!");
        navigationMenuPage.submitDraftButton();
        driver.navigate().refresh();
        Assert.assertFalse(isMailInTheFolder(PATH, TITLE, savedMailTheme),
                "Mail presents in ‘Draft’ folder!");
    }

    @Test(priority = 3)
    public void testLogOut() {
        navigationMenuPage.logOut();
        Assert.assertTrue(LogInPage.loginButton.isDisplayed(), "Login is not successful.");
    }

    @AfterClass
    public void clearDown() {
        logInPage = null;
        navigationMenuPage = null;
        writeEmailPage = null;
    }
}
