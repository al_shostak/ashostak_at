package com.training.at3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SentMailPage extends Page {
    @FindBy(css = "span[title*='Hi']")
    protected static WebElement myEmail;

    public SentMailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }
}
