package com.training.at3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DraftMailPage extends Page {
    @FindBy(css = "span[title*='Hi']")
    private WebElement myEmail;

    @FindBy(css = "span[class^=\"mail-MessageSnippet-FromText\"]")
    protected static WebElement verifyAddress;

    @FindBy(css = "input[value*='Hi']")
    protected static WebElement myThemeEmail;

    public DraftMailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
