package com.training.at3.pf;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://yandex.by/";

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
        }
    }

    protected boolean isMailInTheFolder(String path, String attrName, String value) {
        boolean isPresent = false;
        List<WebElement> webElements = driver.findElements(By.cssSelector(path));
        for (WebElement webElement : webElements) {
            if (webElement.getAttribute(attrName).equals(value)) {
                webElement.click();
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    @AfterClass
    public void reset() {
        driver.quit();
        driver = null;
    }
}