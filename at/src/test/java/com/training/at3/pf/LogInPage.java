package com.training.at3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage extends Page {
    @FindBy(xpath = "//a[contains(@class,'button desk')]")
    protected static WebElement loginButton;

    @FindBy(id = "passp-field-login")
    private WebElement enterEmail;

    @FindBy(xpath = "//button[contains(@type,'submit')]")
    private WebElement submitButton;

    @FindBy(id = "passp-field-passwd")
    private WebElement password;

    public LogInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public LogInPage submitLogInButton() {
        loginButton.click();
        return new LogInPage(driver);
    }

    public LogInPage enterEmail(String email) {
        enterEmail.clear();
        enterEmail.sendKeys(email);
        submitButton.click();
        return new LogInPage(driver);
    }

    public NavigationMenuPage enterPassword(String myPassword) {
        password.clear();
        password.sendKeys(myPassword);
        submitButton.click();
        return new NavigationMenuPage(driver);
    }
}
