package com.training.at9.utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static com.training.at9.browser.Browser.getWebDriverInstance;

public class Helper {

    public static void saveScreenShot(String fileN) {
        WebDriver driver = getWebDriverInstance();
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy-h-mm-ss-SS--a");
        String formattedDate = sdf.format(date);
        String fileName = (fileN.isEmpty() ? "screenshot-" : fileN) + formattedDate;
        try {
            FileUtils.copyFile(scrFile, new File(String.format("./%s.png", fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void highlightElement(WebElement element) {
        WebDriver driver = getWebDriverInstance();
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='5px solid gray'", element);
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.backgroundColor = '" + "yellow" + "'", element);
        }
    }

    public static void waitForElementVisible(By locator) {
        WebDriver driver = getWebDriverInstance();
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
