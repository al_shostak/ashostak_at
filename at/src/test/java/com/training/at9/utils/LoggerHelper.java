package com.training.at9.utils;

import org.apache.log4j.Logger;

public class LoggerHelper {
    private static boolean isExist = false;

    public static Logger getLogger(Class clazz){
        if(isExist){
            return  Logger.getLogger(clazz);
        }
        isExist = true;
        return Logger.getLogger(clazz);
    }
}
