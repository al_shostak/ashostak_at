package com.training.at9.service;

import com.training.at9.po.*;

public class UserActionsService extends AbstractPage {

    public EbayHomePage openEbay() {
        return new EbayHomePage().openEbayHomePage();
    }

    public EbayResultPage searchQuery(String query) {
        return new EbayHomePage().enterQuery(query).clickSearch();
    }

    public void addItemToCart() {
        new EbayResultPage().clickOnFirstProduct();
        new EbayProductPage().addToCart();
    }
}
