package com.training.at9.listener;

import com.training.at9.utils.LoggerHelper;
import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

public class CustomListener extends RunListener {
    private Logger logger = LoggerHelper.getLogger(CustomListener.class);

    @Override
    public void testStarted(Description description){
        logger.info("Listener.testStarted: " + description.getDisplayName());
    }

    @Override
    public void testFinished(Description description){
        logger.info("Listener.testFinished: " + description.getDisplayName());
    }
}
