package com.training.at9.runner;

import com.training.at9.browser.Browser;
import com.training.at9.listener.CustomRunner;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CustomRunner.class)
@CucumberOptions(
        features = "src/test/resources/com/training/at9",
        glue = "",
        tags = "@great"
)
public class RunnerTest {
    @AfterClass
    public static void closeDriver() {
        Browser.close();
    }
}
