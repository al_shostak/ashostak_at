package com.training.at9.steps;

import com.training.at9.po.AbstractPage;
import com.training.at9.service.UserActionsService;
import com.training.at9.utils.LoggerHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import static com.training.at9.po.EbayResultPage.getNameOfFirstProduct;
import static com.training.at9.po.ShoppingCartPage.getNameOfProduct;
import static com.training.at9.utils.Helper.saveScreenShot;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class MyStepdefs extends AbstractPage {
    private Logger logger = LoggerHelper.getLogger(MyStepdefs.class);

    @Given("^I opened www\\.ebay\\.com$")
    public void iOpenedWwwEbayCom() {
        new UserActionsService().openEbay();
        logger.info("Go to the base page ");
        saveScreenShot("./target/screenshots/");
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String product) {
        logger.debug("Starting search the product: " + product);
        new UserActionsService().searchQuery(product);
        saveScreenShot("./target/screenshots/");
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryRequestShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        assertThat(getNameOfFirstProduct().toLowerCase(), containsString(expectPhrase));
        logger.info(expectPhrase + " is the first product in the list");
        saveScreenShot("./target/screenshots/");
    }

    @When("^I add product to the cart$")
    public void iAddTheToTheCart() {
        logger.debug("Trying to add chosen product to the cart");
        new UserActionsService().addItemToCart();
        saveScreenShot("./target/screenshots/");
    }

    @Then("^\"([^\"]*)\" appears in cart$")
    public void appearsInCart(String expectPhrase) {
        assertThat(getNameOfProduct().toLowerCase(), containsString(expectPhrase));
        logger.info(expectPhrase + " in the cart");
        saveScreenShot("./target/screenshots/");
    }
}
