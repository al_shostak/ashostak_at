package com.training.at9.po;

import org.openqa.selenium.By;
import static com.training.at9.browser.Browser.getWebDriverInstance;

public class EbayResultPage extends AbstractPage {
    private static final By LIST_OF_PRODUCTS = By.cssSelector("h3[class^='s-item']");

    public EbayProductPage clickOnFirstProduct() {
        driver.findElements(LIST_OF_PRODUCTS).get(0).click();
        return new EbayProductPage();
    }

    public static String getNameOfFirstProduct() {
        return getWebDriverInstance().findElements(LIST_OF_PRODUCTS).get(0).getText();
    }
}
