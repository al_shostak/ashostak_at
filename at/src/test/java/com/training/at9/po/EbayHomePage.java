package com.training.at9.po;

import org.openqa.selenium.By;
import static com.training.at9.utils.Helper.waitForElementVisible;

public class EbayHomePage extends AbstractPage {
    private static final String BASE_URL = "https://www.ebay.com/";
    private static final By SEARCH_FIELD = By.id("gh-ac");
    private static final By SEARCH_BUTTON = By.id("gh-btn");

    public EbayHomePage openEbayHomePage() {
        driver.get(BASE_URL);
        return this;
    }

    public EbayHomePage enterQuery(String query) {
        waitForElementVisible(SEARCH_FIELD);
        driver.findElement(SEARCH_FIELD).sendKeys(query);
        return new EbayHomePage();
    }

    public EbayResultPage clickSearch() {
        waitForElementVisible(SEARCH_BUTTON);
        driver.findElement(SEARCH_BUTTON).click();
        return new EbayResultPage();
    }
}
