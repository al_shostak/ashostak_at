package com.training.at9.po;;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import static com.training.at9.browser.Browser.getWebDriverInstance;
import static com.training.at9.utils.Helper.highlightElement;
import static com.training.at9.utils.Helper.waitForElementVisible;

public class ShoppingCartPage extends AbstractPage {
    private static final By MY_ITEM = By.className("listsummary-content");

    public static String getNameOfProduct() {
        waitForElementVisible(MY_ITEM);
        List<WebElement> webElements = getWebDriverInstance().findElements(MY_ITEM);
        highlightElement(webElements.get(0));
        return webElements.get(0).getText();
    }
}

