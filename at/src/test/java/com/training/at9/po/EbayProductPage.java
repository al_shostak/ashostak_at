package com.training.at9.po;

import org.openqa.selenium.By;
import static com.training.at9.utils.Helper.waitForElementVisible;

public class EbayProductPage extends AbstractPage {
    private static final By ADD_TO_CART = By.id("isCartBtn_btn");

    public ShoppingCartPage addToCart(){
        waitForElementVisible(ADD_TO_CART);
        driver.findElement(ADD_TO_CART).click();
        return new ShoppingCartPage();
    }
}
