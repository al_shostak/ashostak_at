package com.training.at5.selenide.po;

import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class LogInPage {
    public static final By LOGIN_BUTTON = By.cssSelector("a[data-bem='{\"button\":{}}']");
    private static final By ENTER_EMAIL = By.id("passp-field-login");
    private static final By SUBMIT_BUTTON = By.xpath("//button[contains(@type,'submit')]");
    private static final By PASSWORD = By.id("passp-field-passwd");

    public LogInPage submitLogInButton() {
        $(LOGIN_BUTTON).click();
        return new LogInPage();
    }

    public LogInPage enterEmail(String email) {
        $(ENTER_EMAIL).clear();
        $(ENTER_EMAIL).setValue(email);
        $(SUBMIT_BUTTON).click();
        return new LogInPage();
    }

    public void enterPassword(String myPassword) {
        $(PASSWORD).clear();
        $(PASSWORD).setValue(myPassword);
        $(SUBMIT_BUTTON).click();
    }
}
