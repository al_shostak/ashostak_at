package com.training.at5.selenide.service;

import com.training.at5.selenide.bo.Mail;
import com.training.at5.selenide.bo.User;
import com.training.at5.selenide.po.LogInPage;
import com.training.at5.selenide.po.NavigationMenuPage;
import com.training.at5.selenide.po.WriteEmailPage;

public class UserActionsService {

    public void logInMail(User user) {
        LogInPage logIn = new LogInPage();
        logIn.submitLogInButton().
                enterEmail(user.getEmail()).
                enterPassword(user.getPassword());
    }

    public void writeEmail(Mail mail) {
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage();
        navigationMenuPage.openWriteEmailPage();
        WriteEmailPage writeEmailPage = new WriteEmailPage();
        writeEmailPage.enterAddress(mail.getAddress());
        mail.setTheme(writeEmailPage.enterTheme(mail.getTheme()));
        writeEmailPage.enterText(mail.getBody());
    }

    public String sendEmail(Mail mail) {
        writeEmail(mail);
        WriteEmailPage writeEmailPage = new WriteEmailPage();
        writeEmailPage.sendEmail();
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage();
        navigationMenuPage.openSentEmailPage();
        return mail.getTheme();
    }

    public String saveEmail(Mail mail) {
        writeEmail(mail);
        WriteEmailPage writeEmailPage = new WriteEmailPage();
        writeEmailPage.saveEmailToDraft();
        return mail.getTheme();
    }

    public void checkOutToDraft(){
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage();
        navigationMenuPage.openDraftMailPage();
    }

    public void logOut() {
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage();
        navigationMenuPage.logOut();
    }
}
