package com.training.at5.selenide.bo;

import java.util.Objects;

public class Mail {
    private String address;
    private String theme;
    private String body;

    public Mail(String address, String theme, String body) {
        this.address = address;
        this.theme = theme;
        this.body = body;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mail mail = (Mail) o;
        return Objects.equals(getAddress(), mail.getAddress()) &&
                Objects.equals(getTheme(), mail.getTheme()) &&
                Objects.equals(getBody(), mail.getBody());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAddress(), getTheme(), getBody());
    }

    @Override
    public String toString() {
        return "Mail{" +
                "address='" + address + '\'' +
                ", theme='" + theme + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
