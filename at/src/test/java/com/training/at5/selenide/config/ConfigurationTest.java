package com.training.at5.selenide.config;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import static com.codeborne.selenide.Condition.disappears;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public class ConfigurationTest {
    private static final String TEST_URL = "https://yandex.by/";

    @BeforeClass
    public static void setUp() {
        timeout = 10000;
        startMaximized = true;
        browser = "chrome";
        open(TEST_URL);
        waitUntilPagesIsLoaded();
    }

    protected static void waitUntilPagesIsLoaded() {
        $(byText("Loading")).
                waitUntil(disappears, 20000);
    }

    @AfterClass
    public void closeUp() {
        closeWebDriver();
    }
}
