package com.training.at5.selenide.utils;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Utils {

    public static String getAttribute(By locator, String name) {
        return $(locator).getAttribute(name);
    }

    public static boolean isMailInTheFolder(By by, String attrName, String value) {
        boolean isPresent = false;
        ElementsCollection elementsCollections = $$(by);
        for (SelenideElement element : elementsCollections) {
            if (element.getAttribute(attrName).equals(value)) {
                element.click();
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    public static String getText(By locator) {
        return $(locator).getText();
    }

    public static String createUniqueTheme(String text) {
        return text + System.currentTimeMillis();
    }
}
