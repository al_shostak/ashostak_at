package com.training.at5.selenide.po;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.*;
import static com.training.at5.selenide.utils.Utils.createUniqueTheme;

public class WriteEmailPage {
    private static final By WRITE_TO = By.name("to");
    private static final By THEME_EMAIL = By.cssSelector("input[class^=\"mail-Compose-Field\"]");
    private static final By TEXT_BOX = By.cssSelector("textarea[dir]");
    private static final By SEND_BUTTON = By.cssSelector("span[class='ui-button-text']");
    private static final By SAVE_AND_GO = By.cssSelector("button[class*='small-action-button ']");
    private static final By DRAFT_BUTTON = By.xpath("//a[@href='#draft']");
    private static final By IMG_EMAIL_IS_SENT = By.cssSelector("div[class='mail-Done-Title js-title-info']");
    public static final By MY_EMAILS = By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span[1]");

    public void enterAddress(String address) {
        $(WRITE_TO).shouldBe(Condition.enabled);
        $(WRITE_TO).setValue(address);
    }

    public String enterTheme(String theme) {
        String newTheme = createUniqueTheme(theme);
        $(THEME_EMAIL).setValue(newTheme);
        return newTheme;
    }

    public void enterText(String text) {
        $(TEXT_BOX).setValue(text);
    }

    public void saveEmailToDraft() {
        $(DRAFT_BUTTON).click();
        $(SAVE_AND_GO).shouldBe(Condition.visible);
        $(SAVE_AND_GO).click();
        $(DRAFT_BUTTON).click();
        refresh();
        $(MY_EMAILS).shouldBe(Condition.visible);
    }

    public void sendEmail() {
        $(SEND_BUTTON).click();
        $(IMG_EMAIL_IS_SENT).shouldBe(Condition.visible);
        $(IMG_EMAIL_IS_SENT);
    }
}
