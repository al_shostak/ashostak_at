package com.training.at5.selenide.po;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.*;

public class NavigationMenuPage {
    private static final By WRITE_EMAIL_BUTTON = By.cssSelector("a[class^=\"mail-ComposeButton\"]");
    private static final By SENT_EMAILS = By.xpath("//a[@href='#sent']");
    private static final By DRAFT_BUTTON = By.xpath("//a[@href='#draft']");
    private static final By RECIPIENT = By.cssSelector("#recipient-1");
    private static final By SIGN_OUT = By.xpath("//a[@data-metric='Sign out of Yandex services']");

    public void openWriteEmailPage() {
        $(WRITE_EMAIL_BUTTON).click();
    }

    public void openSentEmailPage() {
        $(SENT_EMAILS).click();
        refresh();
        $(SENT_EMAILS).shouldBe(Condition.visible);
    }

    public void openDraftMailPage() {
        $(DRAFT_BUTTON).shouldBe(Condition.visible);
        $(DRAFT_BUTTON).click();
        refresh();
    }

    public void logOut() {
        $(RECIPIENT).shouldBe(Condition.visible);
        $(RECIPIENT).click();
        $(SIGN_OUT).click();
    }
}
