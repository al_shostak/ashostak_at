package com.training.at5.selenide.tests;

import com.codeborne.selenide.Condition;
import com.training.at5.selenide.bo.Mail;
import com.training.at5.selenide.bo.User;
import com.training.at5.selenide.service.UserActionsService;
import com.training.at5.selenide.config.ConfigurationTest;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.$;
import static com.training.at5.selenide.po.DraftMailPage.*;
import static com.training.at5.selenide.po.LogInPage.LOGIN_BUTTON;
import static com.training.at5.selenide.utils.Utils.*;

public class EmailTest extends ConfigurationTest {
    private UserActionsService userActions;
    private User user;
    private Mail mail;
    private static final String USER_EMAIL = "at.training2019";
    private static final String USER_PASSWORD = "at.training";
    private static final String ADDRESS = "mogilev@yandex.by";
    private static final String PARTOFADDRESS = "mogilev";
    private static final String THEME = "Hi";
    private static final String BODY = "Hello everybody!";
    private static final String VALUE = "value";
    private static final String TITLE = "title";

    @BeforeClass
    public void mailSetup() {
        userActions = new UserActionsService();
        user = new User(USER_EMAIL, USER_PASSWORD);
        mail = new Mail(ADDRESS, THEME, BODY);
    }

    @Test(priority = 0)
    public void testLogIn() {
        userActions.logInMail(user);
        $(LOGIN_BUTTON).shouldBe(Condition.not(Condition.visible));
    }

    @Test(priority = 1)
    public void testSaveEmail() {
        String expectedTheme = userActions.saveEmail(mail);
        System.out.println(expectedTheme);
        Assert.assertTrue(isMailInTheFolder(MY_EMAILS,TITLE,expectedTheme),
                "Mail does not present in draft folder!");
        Assert.assertTrue(getText(VERIFY_ADDRESS).contains(PARTOFADDRESS),
                "[Address is not the same!]");
        Assert.assertEquals(expectedTheme, getAttribute(MY_THEME_EMAIL, VALUE),
                "[Theme is not the same!]");
    }

    @Test(priority = 2)
    public void testSendEmail() {
        String expectedTheme = userActions.sendEmail(mail);
        System.out.println(expectedTheme);
        Assert.assertTrue(isMailInTheFolder(MY_EMAILS, TITLE, expectedTheme),
                "Mail does not present in sent folder!");
        userActions.checkOutToDraft();
        Assert.assertFalse(isMailInTheFolder(MY_EMAILS, TITLE, expectedTheme),
                "Mail presents in draft folder!");
    }

    @Test(priority = 3)
    public void testLogOut() {
        userActions.logOut();
        $(LOGIN_BUTTON).shouldBe(Condition.visible);
    }

    @AfterClass
    public void mailClear() {
        user = null;
        mail = null;
    }
}
