package com.training.at5.selenium.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentMailPage extends Page {
    public static final By MY_EMAILS = By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span[1]");

    public SentMailPage(WebDriver driver) {
        super(driver);
    }
}


