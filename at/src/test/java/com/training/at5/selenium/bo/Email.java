package com.training.at5.selenium.bo;

import java.util.Objects;

public class Email {
    private String address;
    private String theme;
    private String body;

    public Email(String address, String theme, String body) {
        this.address = address;
        this.theme = theme;
        this.body = body;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(address, email.address) &&
                Objects.equals(theme, email.theme) &&
                Objects.equals(body, email.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, theme, body);
    }

    @Override
    public String toString() {
        return "Email{" +
                "address='" + address + '\'' +
                ", theme='" + theme + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
