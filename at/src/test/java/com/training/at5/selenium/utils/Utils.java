package com.training.at5.selenium.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Utils {

    public static boolean elementIsPresent(WebDriver driver, By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public static String getAttribute(WebDriver driver, By locator, String name) {
        WebElement webElement = driver.findElement(locator);
        return webElement.getAttribute(name);
    }

    public static boolean isMailInTheFolder(WebDriver driver, By by, String attrName, String value) {
        boolean isPresent = false;
        List<WebElement> webElements = driver.findElements(by);
        for (WebElement webElement : webElements) {
            if (webElement.getAttribute(attrName).equals(value)) {
                webElement.click();
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    public static String getText(WebDriver driver, By locator) {
        return driver.findElement(locator).getText();
    }

    public static void waitForElementVisibility(WebDriver driver, By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static String createUniqueTheme(String text) {
        return text + System.currentTimeMillis();
    }
}
