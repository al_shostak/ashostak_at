package com.training.at5.selenium.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static com.training.at5.selenium.utils.Utils.createUniqueTheme;

public class WriteEmailPage extends Page {
    private static final By WRITE_TO = By.name("to");
    private static final By THEME_EMAIL = By.cssSelector("input[class^=\"mail-Compose-Field\"]");
    private static final By TEXT_BOX = By.cssSelector("textarea[dir]");
    private static final By SEND_BUTTON = By.cssSelector("span[class='ui-button-text']");
    private static final By SAVE_AND_GO = By.cssSelector("button[class*='small-action-button ']");
    private static final By DRAFT_BUTTON = By.xpath("//a[@href='#draft']");
    private static final By IMG_EMAIL_IS_SENT = By.cssSelector("div[class='mail-Done-Title js-title-info']");

    public WriteEmailPage(WebDriver driver) {
        super(driver);
    }

    public WriteEmailPage enterAddress(String address) {
        driver.findElement(WRITE_TO).sendKeys(address);
        return new WriteEmailPage(driver);
    }

    public String enterTheme(String theme) {
        String newTheme = createUniqueTheme(theme);
        driver.findElement(THEME_EMAIL).sendKeys(newTheme);
        return newTheme;
    }

    public WriteEmailPage enterText(String text) {
        driver.findElement(TEXT_BOX).sendKeys(text);
        return new WriteEmailPage(driver);
    }

    public DraftMailPage saveEmailToDraft() {
        driver.findElement(DRAFT_BUTTON).click();
        driver.findElement(SAVE_AND_GO).click();
        driver.navigate().refresh();
        return new DraftMailPage(driver);
    }

    public NavigationMenuPage sendEmail() {
        driver.findElement(SEND_BUTTON).click();
        driver.findElement(IMG_EMAIL_IS_SENT);
        return new NavigationMenuPage(driver);
    }
}
