package com.training.at5.selenium.service;

import com.training.at5.selenium.bo.Email;
import com.training.at5.selenium.bo.User;
import com.training.at5.selenium.po.LogInPage;
import com.training.at5.selenium.po.NavigationMenuPage;
import com.training.at5.selenium.po.WriteEmailPage;
import com.training.at5.selenium.po.Page;
import org.openqa.selenium.WebDriver;

public class UserActionsService extends Page {

    public UserActionsService(WebDriver driver) {
        super(driver);
    }

    public void logInMail(User user) {
        LogInPage logIn = new LogInPage(driver);
        logIn.submitLogInButton().
                enterEmail(user.getEmail()).
                enterPassword(user.getPassword());
    }

    public void writeEmail(Email email) {
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage(driver);
        navigationMenuPage.openWriteMailPage();
        WriteEmailPage writeEmailPage = new WriteEmailPage(driver);
        writeEmailPage.enterAddress(email.getAddress());
        email.setTheme(writeEmailPage.enterTheme(email.getTheme()));
        writeEmailPage.enterText(email.getBody());
    }

    public String sendEmail(Email email) {
        WriteEmailPage writeEmailPage = new WriteEmailPage(driver);
        writeEmailPage.sendEmail();
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage(driver);
        navigationMenuPage.openSentMailPage();
        return email.getTheme();
    }

    public String saveEmail(Email email) {
        WriteEmailPage writeEmailPage = new WriteEmailPage(driver);
        writeEmailPage.saveEmailToDraft();
        return email.getTheme();
    }

    public void checkOutToDraft() {
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage(driver);
        navigationMenuPage.openDraftMailPage();
    }

    public void logOut() {
        NavigationMenuPage navigationMenuPage = new NavigationMenuPage(driver);
        navigationMenuPage.logOut();
    }
}
