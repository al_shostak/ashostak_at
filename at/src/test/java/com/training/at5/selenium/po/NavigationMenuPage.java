package com.training.at5.selenium.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static com.training.at5.selenium.utils.Utils.waitForElementVisibility;

public class NavigationMenuPage extends Page {
    private static final By WRITE_EMAIL_BUTTON = By.cssSelector("a[class^=\"mail-ComposeButton\"]");
    private static final By SENT_EMAILS = By.xpath("//a[@href='#sent']");
    private static final By DRAFT_BUTTON = By.xpath("//a[@href='#draft']");
    private static final By RECIPIENT = By.cssSelector("#recipient-1");
    private static final By SIGN_OUT = By.xpath("//a[@data-metric='Sign out of Yandex services']");

    public NavigationMenuPage(WebDriver driver) {
        super(driver);
    }

    public WriteEmailPage openWriteMailPage() {
        driver.findElement(WRITE_EMAIL_BUTTON).click();
        return new WriteEmailPage(driver);
    }

    public SentMailPage openSentMailPage() {
        waitForElementVisibility(driver,SENT_EMAILS);
        driver.findElement(SENT_EMAILS).click();
        driver.navigate().refresh();
        return new SentMailPage(driver);
    }

    public DraftMailPage openDraftMailPage() {
        waitForElementVisibility(driver,DRAFT_BUTTON);
        driver.findElement(DRAFT_BUTTON).click();
        driver.navigate().refresh();
        return new DraftMailPage(driver);
    }

    public void logOut() {
        waitForElementVisibility(driver,RECIPIENT);
        driver.findElement(RECIPIENT).click();
        driver.findElement(SIGN_OUT).click();
    }
}
