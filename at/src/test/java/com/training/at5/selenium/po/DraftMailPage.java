package com.training.at5.selenium.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftMailPage extends Page {
    public static final By MY_EMAILS = By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span[1]");
    public static final By VERIFY_ADDRESS = By.cssSelector("span[class^=\"mail-MessageSnippet-FromText\"]");
    public static final By MY_THEME_EMAIL = By.xpath("//input[contains(@class,'mail-Compose-Field')]");

    public DraftMailPage(WebDriver driver) {
        super(driver);
    }
}
