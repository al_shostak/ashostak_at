package com.training.at5.selenium.tests;

import com.training.at5.selenium.bo.Email;
import com.training.at5.selenium.bo.User;
import com.training.at5.selenium.po.DraftMailPage;
import com.training.at5.selenium.po.SentMailPage;
import com.training.at5.selenium.service.UserActionsService;
import com.training.at5.selenium.config.ConfigurationTest;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.training.at5.selenium.po.LogInPage.LOGIN_BUTTON;
import static com.training.at5.selenium.utils.Utils.*;

public class MailTest extends ConfigurationTest {
    private UserActionsService userActions;
    private User user;
    private Email email;
    private static final String USER_EMAIL = "at.training2019";
    private static final String USER_PASSWORD = "at.training";
    private static final String ADDRESS = "minsk@yandex.by";
    private static final String PARTOFADDRESS = "minsk";
    private static final String THEME = "Hi";
    private static final String BODY = "Hello everybody!";
    private static final String VALUE = "value";
    private static final String TITLE = "title";

    @BeforeClass
    public void mailSetup() {
        userActions = new UserActionsService(driver);
        user = new User(USER_EMAIL, USER_PASSWORD);
        email = new Email(ADDRESS, THEME, BODY);
    }

    @Test(priority = 0)
    public void testLogIn() {
        userActions.logInMail(user);
        Assert.assertFalse(elementIsPresent(driver,LOGIN_BUTTON));
    }

    @Test(priority = 1)
    public void testSaveEmail() {
        userActions.writeEmail(email);
        String expectedTheme = userActions.saveEmail(email);
        Assert.assertTrue(isMailInTheFolder(driver, DraftMailPage.MY_EMAILS, TITLE, expectedTheme),
                "Mail does not present in draft folder!");
        Assert.assertTrue(getText(driver,DraftMailPage.VERIFY_ADDRESS).contains(PARTOFADDRESS),
                "[Address is not the same!]");
        Assert.assertEquals(expectedTheme, getAttribute(driver,DraftMailPage.MY_THEME_EMAIL, VALUE),
                "[Theme is not the same!]");
    }

    @Test(priority = 2)
    public void testSendEmail() {
        String expectedTheme = userActions.sendEmail(email);
        Assert.assertTrue(isMailInTheFolder(driver,SentMailPage.MY_EMAILS, TITLE, expectedTheme),
                "Mail does not present in sent folder!");
        userActions.checkOutToDraft();
        Assert.assertFalse(isMailInTheFolder(driver, DraftMailPage.MY_EMAILS, TITLE, expectedTheme),
                "Mail presents in draft folder!");
    }

    @Test(priority = 3)
    public void testLogOut() {
        userActions.logOut();
        Assert.assertTrue(elementIsPresent(driver,LOGIN_BUTTON));
    }

    @AfterClass
    public void mailClear() {
        userActions = null;
        user = null;
        email = null;
    }
}
